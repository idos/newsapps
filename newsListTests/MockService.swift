//
//  MockService.swift
//  newsListTests
//
//  Created by R+IOS on 20/02/22.
//

import Foundation

class MockService: ServiceProtocol {
    var mockData: [News]?
    
    init(mockData: [News]?) {
        self.mockData = mockData
    }
    
    func fetchData(completion: @escaping ([News]?) -> Void) {
        completion(mockData)
    }
    
}
