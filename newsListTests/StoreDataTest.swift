//
//  StoreDataTest.swift
//  newsListTests
//
//  Created by R+IOS on 20/02/22.
//

import Foundation
import XCTest

class StoreDataTest: XCTestCase {
    
    func testSaveDataFavorite() {
        let localData = StoreData()
        let news = News.with()
        
        localData.saveDataFavorite(news: news)
    }
    
    func testRemoveDataFavorite() {
        let localData = StoreData()
        let news = News.with()
        
        localData.removeFavorite(news: news)
    }
}
