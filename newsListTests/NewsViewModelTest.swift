//
//  NewsViewModelTest.swift
//  newsListTests
//
//  Created by R+IOS on 20/02/22.
//

import XCTest
@testable import newsList

class NewsViewModelTest: XCTestCase {
    func testSuccesGetData() {
        let expetedData = [News.with()]
        let serivce = MockService(mockData: expetedData)
        
        let viewModel = NewsViewModel(service: serivce)
        
        viewModel.loadData()
        
        XCTAssertFalse(viewModel.isLoad)
        XCTAssertTrue(!viewModel.isFavorite)
        XCTAssertEqual(viewModel.news.articles.count, 1)
    }
    
    func testEmptyData() {
        let expetedData = [News]()
        let serivce = MockService(mockData: expetedData)
        
        let viewModel = NewsViewModel(service: serivce)
        
        viewModel.loadData()
        
        XCTAssertFalse(viewModel.isLoad)
        XCTAssertTrue(!viewModel.isFavorite)
        XCTAssertEqual(viewModel.news.articles.count, 0)
    }
    
    func testNilData() {
        let expetedData: [News]? = nil
        let serivce = MockService(mockData: expetedData)
        
        let viewModel = NewsViewModel(service: serivce)
        
        viewModel.loadData()
        
        XCTAssertFalse(viewModel.isLoad)
        XCTAssertTrue(!viewModel.isFavorite)
        XCTAssertEqual(viewModel.news.articles.count, 0)
    }
}
