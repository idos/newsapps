//
//  ListNewsTest.swift
//  newsListTests
//
//  Created by R+IOS on 20/02/22.
//

import XCTest
@testable import newsList

class ListNewsTest: XCTestCase {
    func testSuccessParse() {
        let json = """
        {
            "articles":[
                  {
                     "source":{
                        "id":"wired",
                        "name":"Wired"
                     },
                     "author":"Louryn Strampe, Gear Team",
                     "title":"The 20 Best Presidents' Day Sales on Home Goods and Tech",
                     "description":"Make the most of your long weekend with deals for every room of the house—including the backyard.",
                     "url":"https://www.wired.com/story/presidents-day-sales-2022/",
                     "urlToImage":"https://media.wired.com/photos/5fd14e1c5310f0846594ca50/191:100/w_1280,c_limit/Gear-Roborock-S4-Max.jpg",
                     "publishedAt":"2022-02-19T12:00:00Z",
                     "content":"Holiday weekend sales, including Presidents Day, can be lackluster. A discounted price that's already been available for months isn't really a deal. And don't get me started on prices that get marked… [+9013 chars]"
                  }
            ]
        }
        """.data(using: .utf8)!
        
        let news = try! JSONDecoder().decode(ListNews.self, from: json)
        
        XCTAssertNotNil(news)
        XCTAssertEqual(news.articles.first?.title, News.with().title)
    }
}

