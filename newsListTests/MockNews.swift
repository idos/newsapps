//
//  MockNews.swift
//  newsListTests
//
//  Created by R+IOS on 20/02/22.
//

import Foundation


extension News {
    static func with(
        source: Source = Source(id: "wired", name: "wired"),
        author: String = "Louryn Strampe, Gear Team",
        title: String = "The 20 Best Presidents' Day Sales on Home Goods and Tech",
        description: String = "Make the most of your long weekend with deals for every room of the house—including the backyard.",
        url: String = "https://www.wired.com/story/presidents-day-sales-2022/",
        urlToImage: String = "https://media.wired.com/photos/5fd14e1c5310f0846594ca50/191:100/w_1280,c_limit/Gear-Roborock-S4-Max.jpg",
        publishedAt: String = "2022-02-19T12:00:00Z",
        content: String = "Holiday weekend sales, including Presidents Day, can be lackluster. A discounted price that's already been available for months isn't really a deal. And don't get me started on prices that get marked… [+9013 chars]"
    ) -> News {
        return News(source: source, author: author, title: title, description: description, url: url, urlToImage: urlToImage, publishedAt: publishedAt, content: content)
    }
}
