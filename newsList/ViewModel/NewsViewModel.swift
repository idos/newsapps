//
//  NewsViewModel.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation
import Combine

class NewsViewModel: ObservableObject {
    @Published var news = ListNews(articles: [])
    @Published var isLoad = false
    @Published var isFavorite = false
    
    let service: ServiceProtocol
    let localData = StoreData()
    
    init(service: ServiceProtocol = Service()) {
        self.service = service
    }
    
    func loadData() {
        isLoad = true
        
        if isFavorite {
            isLoad = false
            let data = localData.getFavorite()
            
            self.news.articles = data
        } else {
            service.fetchData { news in
                self.isLoad = false
                
                guard let newsList = news else {
                    return
                }
                
                self.news.articles = newsList
            }
        }
    }
}
