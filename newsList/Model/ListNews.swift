//
//  ListNews.swift
//  newsList
//
//  Created by R+IOS on 19/02/22.
//

import Foundation

struct ListNews: Codable {
    var articles: [News]
}
