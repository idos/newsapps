//
//  News.swift
//  newsList
//
//  Created by R+IOS on 19/02/22.
//

import Foundation

struct Source: Codable {
    var id: String?
    var name: String?
}

struct News: Codable {
    var source: Source
    var author: String?
    var title: String
    var description: String
    var url: String
    var urlToImage: String?
    var publishedAt: String
    var content: String
}
