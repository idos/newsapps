//
//  DetailsNews.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct NewsDetails: View {
    var news: News
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Spacer()
            
            WebImage(url: URL(string: news.urlToImage ?? ""))
                .resizable()
                .frame(
                    width: UIScreen.main.bounds.width - 20,
                    height: UIScreen.main.bounds.width - 40
                )
            
            Text("Author: \(news.author ?? "")")
                .font(.callout)
                .lineLimit(nil)
            
            Link(destination: URL(string: news.url)!) {
                Text("click here")
                    .font(.caption)
                    .foregroundColor(.blue)
            }
            
            Text("\(news.description)")
                .font(.body)
                .lineLimit(nil)
            
            Text("\(news.content)")
                .font(.body)
                .lineLimit(nil)
            
            Spacer()
            
        }
        .navigationBarTitle(
            Text(news.title),
            displayMode: .inline
        )
        .padding([.leading, .trailing], 20)
    }
}

struct NewsDetails_Previews: PreviewProvider {
    static var previews: some View {
        NewsDetails(news: News.with())
    }
}
