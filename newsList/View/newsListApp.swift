//
//  newsListApp.swift
//  newsList
//
//  Created by R+IOS on 19/02/22.
//

import SwiftUI

@main
struct newsListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
