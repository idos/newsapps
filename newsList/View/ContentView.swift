//
//  ContentView.swift
//  newsList
//
//  Created by R+IOS on 19/02/22.
//

import SwiftUI
import Combine

struct ContentView: View {
    
    var body: some View {
        NavigationView {
            TabView {
                ListNewsView()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Home")
                    }
                
                ListNewsView(isFavorite: true)
                    .tabItem {
                        Image(systemName: "bookmark.circle.fill")
                        Text("Favorite")
                    }
            }
            .accentColor(.red)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
