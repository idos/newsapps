//
//  NewsRow.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct NewsRow: View {
    var news: News
    var isFavorite: Bool = false
    
    var body: some View {
        HStack {
            WebImage(url: URL(string: news.urlToImage ?? ""))
                .resizable()
                .indicator(Indicator.progress)
                .frame(width: 100, height: 120)
            
            VStack(alignment: .leading) {
                Spacer()
                
                Text("Title: \(news.title)")
                    .font(.body)
                    .foregroundColor(.blue)
                    .lineLimit(3)
                
                Text("Author: \(news.author ?? "")")
                    .foregroundColor(.gray)
                    .lineLimit(3)
                
                HStack(spacing: 3) {
                    Spacer()
                    
                    Text("Become favorite")
                        .foregroundColor(.gray)
                    
                    Image(systemName: self.isFavorite ? "heart.fill" : "heart")
                        .foregroundColor(.red)
                    
                    Spacer()
                }
                .frame(height: 30)
                .border(.gray, width: 1)
                .cornerRadius(5)
                .padding([.all], 10)
                .onTapGesture {
                    if !isFavorite {
                        StoreData().saveDataFavorite(news: self.news)
                    } else {
                        StoreData().removeFavorite(news: self.news)
                    }
                }
                
                Spacer()
            }
        }.frame(height: 150)
            .foregroundColor(.blue)
    }
}

struct NewsRow_Previews: PreviewProvider {
    static var previews: some View {
        NewsRow(news: News.with())
    }
}
