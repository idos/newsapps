//
//  ListNewsView.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation
import SwiftUI

struct ListNewsView: View {
    @ObservedObject var viewmodel = NewsViewModel()
    
    var isFavorite = false
    
    var body: some View {
        VStack(alignment: .center) {
            if viewmodel.isLoad {
                ActivityIndicator(color: .blue, size: 60)
            } else {
                if viewmodel.news.articles.count > 0 {
                    List(viewmodel.news.articles, id: \.title) { news in
                        
                        NavigationLink {
                            NewsDetails(news: news)
                        } label: {
                            NewsRow(news: news, isFavorite: isFavorite)
                        }
                    }
                } else {
                    VStack(alignment: .center) {
                        Text("No news for today")
                    }
                }
            }
        }
        .onAppear {
            if isFavorite {
                viewmodel.isFavorite = true
            } else {
                viewmodel.isFavorite = false
            }
            
            viewmodel.loadData()
        }
        .navigationBarTitle("News")
    }
}
