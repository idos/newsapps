//
//  StoreData.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation

class StoreData {
    func saveDataFavorite(news: News) {
        let listNews = getFavorite()
        var newList: [News] = []
        
        newList.append(news)
        if listNews.count >= 1 {
            for item in listNews {
                if item.title != news.title {
                    newList.append(item)
                }
            }
        }
        
        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(newList)

            // Write/Set Data
            UserDefaults.standard.set(data, forKey: "favorite")

        } catch {
            print("Unable to Encode Array of News (\(error))")
        }
    }
    
    func getFavorite() -> [News] {
        var listNews: [News] = []
        // Read/Get Data
        if let data = UserDefaults.standard.data(forKey: "favorite") {
            do {
                // Create JSON Decoder
                let decoder = JSONDecoder()

                // Decode Note
                listNews = try decoder.decode([News].self, from: data)

            } catch {
                print("Unable to Decode Notes (\(error))")
            }
        }
        return listNews
    }
    
    func removeFavorite(news: News) {
        let listNews = getFavorite()
        var newList: [News] = []
        
        if listNews.count >= 1 {
            for item in listNews {
                if item.title != news.title {
                    newList.append(news)
                }
            }
        }
        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(newList)

            // Write/Set Data
            UserDefaults.standard.set(data, forKey: "favorite")

        } catch {
            print("Unable to Encode Array of News (\(error))")
        }
    }
}
