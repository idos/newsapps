//
//  Service.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation

class Service: ServiceProtocol {
    var url = "https://newsapi.org/v2/everything?q=Apple&from=2022-02-19&sortBy=popularity&apiKey="
    var api_key = "API_KEY"
    
    func fetchData(completion: @escaping ([News]?) -> Void) {
        loadData(completion: completion)
    }
    
    private func loadData(completion: @escaping ([News]?) -> Void) {
        guard let url = URL(string: "\(url)\(api_key)") else { return }
        URLSession.shared.dataTask(with: url){ (data, _, _) in
            guard let data = data else {
                completion(nil)
                return
            }
            guard let news = try? JSONDecoder().decode(ListNews.self, from: data) else {
                completion(nil)
                return
            }
            DispatchQueue.main.async {
                completion(news.articles)
            }
        }.resume()
    }
}
