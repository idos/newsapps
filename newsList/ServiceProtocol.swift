//
//  ServiceProtocol.swift
//  newsList
//
//  Created by R+IOS on 20/02/22.
//

import Foundation

protocol ServiceProtocol {
    func fetchData(completion: @escaping ([News]?) -> Void)
}
