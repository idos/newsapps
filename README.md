# NewsApps

This showing list of news from API https://newsapi.org/.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

- Load the data from server the https://newsapi.org/. You need to register an account and get api key to replace <API_KEY> in the Service.swift file.
- For add favorite click on part "Become Favorite", it will show in next tab menu.
